package com.DemoSyndicateJava;

public class RequestEvent {

    public String getRawPath() {
        return rawPath;
    }

    public void setRawPath(String rawPath) {
        this.rawPath = rawPath;
    }

    private String rawPath;

    public RequestEvent(String rawPath) {
        this.rawPath = rawPath;
    }

    public RequestEvent() {
    }
}



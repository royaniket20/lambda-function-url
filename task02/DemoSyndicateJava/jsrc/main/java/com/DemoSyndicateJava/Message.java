package com.DemoSyndicateJava;

public class Message {

    private Integer statusCode;
    private  String message;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message() {
    }

    public Message(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }
}

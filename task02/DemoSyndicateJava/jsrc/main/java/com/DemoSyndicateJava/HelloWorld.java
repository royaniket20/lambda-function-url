package com.DemoSyndicateJava;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.annotations.lambda.LambdaLayer;
import com.syndicate.deployment.annotations.lambda.LambdaUrlConfig;
import com.syndicate.deployment.model.Architecture;
import com.syndicate.deployment.model.ArtifactExtension;
import com.syndicate.deployment.model.DeploymentRuntime;
import com.syndicate.deployment.model.RetentionSetting;
import com.syndicate.deployment.model.lambda.url.AuthType;
import com.syndicate.deployment.model.lambda.url.InvokeMode;
import org.apache.velocity.exception.ResourceNotFoundException;

import java.util.HashMap;
import java.util.Map;

@LambdaHandler(lambdaName = "hello_world",
        roleName = "hello_world-role",
        layers = {"sdk-layer"},
        isPublishVersion = true,
        aliasName = "${lambdas_alias_name}",
        architecture = Architecture.ARM64,
        logsExpiration = RetentionSetting.SYNDICATE_ALIASES_SPECIFIED
)
@LambdaLayer(
        layerName = "sdk-layer",
        libraries = {"lib/commons-lang3-3.14.0.jar", "lib/gson-2.10.1.jar"},
        runtime = DeploymentRuntime.JAVA11,
        architectures = {Architecture.ARM64},
        artifactExtension = ArtifactExtension.ZIP
)
@LambdaUrlConfig(
        authType = AuthType.NONE,
        invokeMode = InvokeMode.BUFFERED
)
public class HelloWorld implements RequestHandler<Object, String> {

    public String handleRequest(Object request, Context context) {

        String json = new Gson().toJson(request);
        context.getLogger().log("Event Json - "+json);
        RequestEvent requestEvent = new Gson().fromJson(json, RequestEvent.class);
        String path = requestEvent.getRawPath();
        context.getLogger().log("URL PATH = " + path);
        if (path.equals("/hello")) {
            context.getLogger().log("Valid Request is fired !!! ");
           Message msg = new Message(200,"Hello from Lambda");
           String response =  new Gson().toJson(msg);
            context.getLogger().log("Event Response  - "+response);
            return response;
        }else{
            context.getLogger().log("Invalid Request is fired !!! ");
            throw new ResourceNotFoundException("Not calling from Valid Path");
        }
    }

}
